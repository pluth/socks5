use std::sync::Arc;

use socks5::{authentication::Authenticator, load_config, server::serve};

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "debug");
    }
    tracing_subscriber::fmt::init();

    let path = std::env::args()
        .nth(1)
        .unwrap_or("./config.json".to_string());
    let config = Arc::new(load_config(path));

    tracing::info!(?config, "config");

    let auth = Arc::new(Authenticator::new(config.users.clone()));

    serve(auth, config).await
}
