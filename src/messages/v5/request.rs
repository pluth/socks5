use super::address::Address;
use super::SOCKS_VERSION;
use anyhow::anyhow;
use anyhow::bail;
use tokio::io::AsyncRead;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWrite;
use tokio::io::AsyncWriteExt;

const RSV: u8 = 0x0;

#[derive(Debug)]
pub struct ClientConnectionRequest {
    pub cmd: Command,
    pub dstaddr: Address,
    pub dstport: u16,
}

impl ClientConnectionRequest {
    pub async fn read<T: AsyncRead + Unpin>(stream: &mut T) -> anyhow::Result<Self> {
        let mut bytes = [0; 3];
        stream.read_exact(&mut bytes).await?;
        let [ver, cmd, rsv] = bytes;

        if ver != SOCKS_VERSION {
            bail!("socks5 only: {}", ver);
        }
        if rsv != RSV {
            bail!("rsv must be 0: {}", rsv);
        }
        let cmd = Command::parse(cmd).ok_or_else(|| anyhow!("unknown command: {}", cmd))?;

        let dstaddr = Address::read(stream).await?;
        let dstport = stream.read_u16().await?;

        Ok(Self {
            cmd,
            dstaddr,
            dstport,
        })
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ServerStatus {
    Success = 0x00,
    GeneralFailure = 0x01,
}

pub struct ServerConnectionResponse {
    status: ServerStatus,
    bndaddr: Address,
    bndport: u16,
}

impl ServerConnectionResponse {
    pub fn new(status: ServerStatus, bndaddr: Address, bndport: u16) -> Self {
        Self {
            status,
            bndaddr,
            bndport,
        }
    }

    pub async fn write<T: AsyncWrite + Unpin>(&self, mut stream: T) -> anyhow::Result<()> {
        let bytes = [SOCKS_VERSION, self.status as u8, RSV];
        stream.write_all(&bytes).await?;
        self.bndaddr.write(&mut stream).await?;
        stream.write_u16(self.bndport).await?;
        Ok(())
    }
}

#[derive(Debug)]
pub enum Command {
    TcpStream = 0x01,
    TcpPortBind = 0x02,
    UdpPort = 0x03,
}

impl Command {
    pub fn parse(value: u8) -> Option<Self> {
        match value {
            0x01 => Some(Self::TcpStream),
            0x02 => Some(Self::TcpPortBind),
            0x03 => Some(Self::UdpPort),
            _ => None,
        }
    }
}
