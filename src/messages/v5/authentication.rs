use anyhow::bail;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

const AUTH_VERSION: u8 = 0x01;
pub const AUTH_SUCCESS: u8 = 0x00;
pub const AUTH_FAILURE: u8 = 0x01;

#[allow(dead_code)]
#[derive(Debug)]
pub struct ClientAuthRequest {
    pub ver: u8,
    pub id: Box<str>,
    pub pw: Box<str>,
}

impl ClientAuthRequest {
    pub async fn read<T: AsyncRead + Unpin>(stream: &mut T) -> anyhow::Result<Self> {
        let mut bytes = [0u8; 2];

        stream.read_exact(&mut bytes).await?;
        let [ver, idlen] = bytes;

        if ver != AUTH_VERSION {
            bail!("bad version: {}", ver);
        }

        let mut id = String::with_capacity(idlen as usize);
        let mut id_stream = stream.take(idlen as u64);
        id_stream.read_to_string(&mut id).await?;

        let pwlen = stream.read_u8().await?;
        let mut pw = String::with_capacity(pwlen as usize);
        let mut pw_stream = stream.take(pwlen as u64);
        pw_stream.read_to_string(&mut pw).await?;

        Ok(Self {
            ver,
            id: id.into_boxed_str(),
            pw: pw.into_boxed_str(),
        })
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct ServerResponse {
    ver: u8,
    status: u8,
}

impl ServerResponse {
    pub fn new(status: u8) -> Self {
        Self {
            ver: AUTH_VERSION,
            status,
        }
    }

    pub async fn write<T: AsyncWrite + Unpin>(&self, mut stream: T) -> anyhow::Result<()> {
        let bytes = [self.ver, self.status];
        stream.write_all(&bytes).await?;
        Ok(())
    }
}
