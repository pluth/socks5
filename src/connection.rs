use std::{io, net::SocketAddr, sync::Arc};

use anyhow::bail;
use tokio::{
    io::{BufReader, BufWriter},
    net::{
        lookup_host,
        tcp::{OwnedReadHalf, OwnedWriteHalf},
        TcpSocket, TcpStream, ToSocketAddrs,
    },
    try_join,
};

use crate::{
    messages::v5::{address::Address, request::Command},
    Config, ServerMode,
};

pub struct ClientConnection {
    pub read: BufReader<OwnedReadHalf>,
    pub write: BufWriter<OwnedWriteHalf>,
}

impl ClientConnection {
    pub fn new(read: BufReader<OwnedReadHalf>, write: BufWriter<OwnedWriteHalf>) -> Self {
        Self { read, write }
    }
}

pub async fn proxy(
    command: Command,
    address: Address,
    port: u16,
    config: Arc<Config>,
) -> anyhow::Result<Proxy> {
    match command {
        Command::TcpStream => proxy_tcpstream(address, port, config).await,
        _ => bail!("command not implemented"),
    }
}

async fn connect<T: ToSocketAddrs>(address: T, config: &Config) -> anyhow::Result<TcpStream> {
    let mut addrs = lookup_host(address).await?.collect::<Vec<_>>();

    match config.mode {
        ServerMode::V4 => {
            addrs.retain(|a| a.is_ipv4());
        }
        ServerMode::V6 => {
            addrs.retain(|a| a.is_ipv6());
        }
        ServerMode::V4V6 => {
            addrs.sort_by_key(|a| a.is_ipv6());
        }
        ServerMode::V6V4 => {
            addrs.sort_by_key(|a| a.is_ipv4());
        }
    }

    let mut last_err = None;

    for addr in addrs {
        let socket = match addr {
            SocketAddr::V4(_) => {
                let v4 = TcpSocket::new_v4()?;
                if let Some(bind) = config.source_v4 {
                    v4.bind(bind.into())?;
                }
                v4
            }
            SocketAddr::V6(_) => {
                let v6 = TcpSocket::new_v6()?;
                if let Some(bind) = config.source_v6 {
                    v6.bind(bind.into())?;
                }
                v6
            }
        };

        match socket.connect(addr).await {
            Ok(stream) => return Ok(stream),
            Err(e) => last_err = Some(e),
        }
    }

    Err(last_err
        .unwrap_or_else(|| {
            io::Error::new(
                io::ErrorKind::InvalidInput,
                "could not resolve to any address",
            )
        })
        .into())
}

#[tracing::instrument(skip(config))]
async fn proxy_tcpstream(
    address: Address,
    port: u16,
    config: Arc<Config>,
) -> anyhow::Result<Proxy> {
    //TODO allow/deny ruleset
    let stream = match address {
        Address::Ipv4(v4) => connect((v4, port), &config).await,
        Address::Domain(domain) => connect((domain.as_ref(), port), &config).await,
        Address::Ipv6(v6) => connect((v6, port), &config).await,
    };
    let stream = stream?; //TODO error handling

    let addr = stream.local_addr()?;
    let bndport = addr.port();
    let bndaddr = addr.ip().into();

    tracing::info!(local = ?addr, "connected to dst");

    Ok(Proxy {
        connection: ProxyConnection::TcpStream(stream),
        bndaddr,
        bndport,
    })
}

pub enum ProxyConnection {
    TcpStream(TcpStream),
}

pub struct Proxy {
    pub connection: ProxyConnection,
    pub bndaddr: Address,
    pub bndport: u16,
}

pub async fn do_proxy(
    ClientConnection { mut read, write }: ClientConnection,
    connection: ProxyConnection,
) -> std::io::Result<()> {
    match connection {
        ProxyConnection::TcpStream(stream) => {
            let (proxy_read, mut proxy_write) = stream.into_split();
            let mut proxy_read = BufReader::new(proxy_read);

            let task = Box::pin(async move {
                let mut write = write.into_inner();
                try_join!(
                    tokio::io::copy_buf(&mut proxy_read, &mut write),
                    tokio::io::copy_buf(&mut read, &mut proxy_write)
                )
                .map(|_| ())
            });
            task.await
        }
    }
}
