use std::{
    net::{Ipv4Addr, SocketAddr},
    sync::Arc,
};

use anyhow::{bail, Context};
use tokio::{
    io::{AsyncWriteExt, BufReader, BufWriter},
    net::{TcpListener, TcpStream},
};

use crate::{
    authentication::Authenticator,
    connection::{do_proxy, proxy, ClientConnection},
    messages::v5::{
        address::Address,
        greeting::{ClientGreeting, ServerChoice},
        request::{ClientConnectionRequest, ServerConnectionResponse, ServerStatus},
    },
    Config,
};

pub async fn serve(auth: Arc<Authenticator>, config: Arc<Config>) -> anyhow::Result<()> {
    let server = TcpListener::bind(config.bind).await?;

    let socket_addr = server.local_addr().unwrap();
    tracing::info!(?socket_addr, "bound to");
    loop {
        let (stream, addr) = server.accept().await?;
        let auth = auth.clone();
        let config = config.clone();
        tokio::spawn(connection(auth, stream, addr, config.clone()));
    }
}

#[tracing::instrument(skip(auth, stream, config))]
async fn connection(
    auth: Arc<Authenticator>,
    stream: TcpStream,
    addr: SocketAddr,
    config: Arc<Config>,
) {
    if let Err(e) = socks5_handshake(auth, stream, config).await {
        // let s: String = e.chain().map(|e| e.to_string()).collect();
        let source = e.source();
        tracing::error!(%e, ?source, "connection dropped");
    } else {
        tracing::info!("connection closed");
    };
}

async fn socks5_handshake(
    auth: Arc<Authenticator>,
    stream: TcpStream,
    config: Arc<Config>,
) -> anyhow::Result<()> {
    tracing::info!("new connection");
    let (read, write) = stream.into_split();
    let read = BufReader::new(read);
    let write = BufWriter::with_capacity(128, write);
    let mut client = ClientConnection::new(read, write);

    let auth_method = greeting(&mut client, &auth).await.context("greeting")?;

    auth.authenticate(&mut client, auth_method)
        .await
        .context("auth")?;

    let proxy = connection_request(&mut client, config)
        .await
        .context("request")?;

    do_proxy(client, proxy).await.context("proxy")?;

    Ok(())
}

async fn connection_request(
    client: &mut ClientConnection,
    config: Arc<Config>,
) -> anyhow::Result<crate::connection::ProxyConnection> {
    let request = ClientConnectionRequest::read(&mut client.read).await?;
    tracing::info!(cmd=?request.cmd, addr=?request.dstaddr, port=%request.dstport, "request");
    let proxy = proxy(request.cmd, request.dstaddr, request.dstport, config).await;
    let proxy = match proxy {
        Err(e) => {
            let response = ServerConnectionResponse::new(
                ServerStatus::GeneralFailure,
                Address::Ipv4(Ipv4Addr::UNSPECIFIED),
                0,
            );
            response.write(&mut client.write).await?;
            client.write.flush().await?;
            tracing::error!("failed to create proxy");
            bail!(e);
        }
        Ok(proxy) => {
            let response =
                ServerConnectionResponse::new(ServerStatus::Success, proxy.bndaddr, proxy.bndport);
            response.write(&mut client.write).await?;
            client.write.flush().await?;
            tracing::trace!("proxy created");
            proxy.connection
        }
    };
    Ok(proxy)
}

async fn greeting(
    client: &mut ClientConnection,
    auth: &Arc<Authenticator>,
) -> anyhow::Result<crate::messages::v5::greeting::Auth> {
    let greeting = ClientGreeting::read(&mut client.read).await?;
    tracing::trace!(?greeting, "greeting");
    let auth_method = match auth.choose_method(&greeting) {
        Some(method) => {
            let response = ServerChoice::new(method);
            response.write(&mut client.write).await?;
            client.write.flush().await?;
            method
        }
        None => {
            let response = ServerChoice::fail();
            response.write(&mut client.write).await?;
            client.write.flush().await?;
            anyhow::bail!("only supports username/password");
        }
    };
    Ok(auth_method)
}
