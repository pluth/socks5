use std::collections::HashMap;

use anyhow::bail;
use tokio::io::AsyncWriteExt;

use crate::{
    connection::ClientConnection,
    messages::v5::{
        authentication::{ClientAuthRequest, ServerResponse, AUTH_FAILURE, AUTH_SUCCESS},
        greeting::Auth,
        greeting::ClientGreeting,
    },
};

pub struct Authenticator {
    db: HashMap<String, String>,
}

impl Authenticator {
    pub fn new(db: HashMap<String, String>) -> Self {
        Self { db }
    }

    pub fn choose_method(&self, greeting: &ClientGreeting) -> Option<Auth> {
        if greeting.auth.contains(&Auth::Normal) {
            Some(Auth::Normal)
        } else {
            None
        }
    }

    pub async fn authenticate(
        &self,
        client: &mut ClientConnection,
        method: Auth,
    ) -> anyhow::Result<()> {
        tracing::debug!(?method, "chosen authentication");
        match method {
            Auth::Normal => normal_authenticate(&self.db, client).await,
            _ => bail!("unsupported method: {:?}", method),
        }
    }
}

async fn normal_authenticate(
    db: &HashMap<String, String>,
    client: &mut ClientConnection,
) -> anyhow::Result<()> {
    let read = &mut client.read;
    let write = &mut client.write;

    let auth = ClientAuthRequest::read(read).await?;
    tracing::trace!(id=%auth.id, "normal auth request");

    match db.get(auth.id.as_ref()) {
        Some(pw) if pw.as_str() == auth.pw.as_ref() => {
            tracing::trace!(id=%auth.id, "authenticated");
            let response = ServerResponse::new(AUTH_SUCCESS);
            response.write(&mut *write).await?;
            write.flush().await?;
            Ok(())
        }
        _ => {
            tracing::trace!(id=%auth.id, "not authenticated");
            let response = ServerResponse::new(AUTH_FAILURE);
            response.write(&mut *write).await?;
            write.flush().await?;
            bail!("authentication failure");
        }
    }
}
