pub mod v5 {

    pub const SOCKS_VERSION: u8 = 0x5;

    pub mod address;
    pub mod authentication;
    pub mod greeting;
    pub mod request;
}
