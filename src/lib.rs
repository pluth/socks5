use std::{
    collections::HashMap,
    net::{SocketAddr, SocketAddrV4, SocketAddrV6},
    path::Path,
};

pub mod authentication;
mod connection;
mod messages;
pub mod server;

fn default_bind() -> SocketAddr {
    "[::]:10080".parse().unwrap()
}

#[derive(Debug, Default, serde::Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum ServerMode {
    V4,
    V6,
    V4V6,
    #[default]
    V6V4,
}

#[derive(Debug, serde::Deserialize)]
pub struct Config {
    #[serde(default = "default_bind")]
    pub bind: SocketAddr,
    #[serde(default)]
    pub users: HashMap<String, String>,
    pub source_v4: Option<SocketAddrV4>,
    pub source_v6: Option<SocketAddrV6>,
    #[serde(default)]
    pub mode: ServerMode,
}

fn load_config_file<T: AsRef<Path>>(path: T) -> anyhow::Result<Config> {
    let config = std::fs::read(path)?;
    let config: Config = serde_json::from_slice(&config)?;
    Ok(config)
}

pub fn load_config<T: AsRef<Path>>(path: T) -> Config {
    load_config_file(path).expect("config error")
}
