use anyhow::bail;
use tokio::io::AsyncRead;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWrite;
use tokio::io::AsyncWriteExt;

use super::SOCKS_VERSION;

#[repr(u8)]
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
#[allow(clippy::enum_variant_names)]
pub enum Auth {
    NoAuth = 0x0,
    GssApi = 0x1,
    Normal = 0x2,
}

impl Auth {
    fn parse(value: u8) -> Option<Self> {
        use Auth::*;
        let value = match value {
            0x0 => NoAuth,
            0x01 => GssApi,
            0x02 => Normal,
            _ => return None,
        };
        Some(value)
    }
}

#[derive(Debug)]
pub struct ClientGreeting {
    pub auth: Box<[Auth]>,
}

impl ClientGreeting {
    pub async fn read<T: AsyncRead + Unpin>(mut stream: T) -> anyhow::Result<ClientGreeting> {
        let mut bytes = [0u8; 2];

        stream.read_exact(&mut bytes).await?;
        let [ver, nauth] = bytes;

        if ver != 0x5 {
            bail!("socks5 only: {}", ver);
        }

        let mut auth = Vec::with_capacity(nauth as usize);
        let mut stream = stream.take(nauth as u64);
        stream.read_to_end(&mut auth).await?;

        let auth: Vec<Auth> = auth.into_iter().filter_map(Auth::parse).collect();

        Ok(ClientGreeting {
            auth: auth.into_boxed_slice(),
        })
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct ServerChoice {
    ver: u8,
    cauth: u8,
}

impl ServerChoice {
    const NO_SUPPORTED_AUTH: u8 = 0xff;

    pub fn new(cauth: Auth) -> Self {
        Self {
            ver: SOCKS_VERSION,
            cauth: cauth as u8,
        }
    }

    pub fn fail() -> Self {
        Self {
            ver: SOCKS_VERSION,
            cauth: Self::NO_SUPPORTED_AUTH,
        }
    }

    pub async fn write<T: AsyncWrite + Unpin>(&self, mut stream: T) -> anyhow::Result<()> {
        let bytes = [self.ver, self.cauth];
        stream.write_all(&bytes).await?;
        Ok(())
    }
}
