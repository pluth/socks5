use std::{
    convert::TryInto,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
};

use anyhow::bail;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

#[derive(Debug)]
pub enum Address {
    Ipv4(Ipv4Addr),
    Domain(Box<str>),
    Ipv6(Ipv6Addr),
}

impl Address {
    async fn read_v4<T: AsyncRead + Unpin>(stream: &mut T) -> anyhow::Result<Self> {
        let mut bytes = [0; 16];
        stream.read_exact(&mut bytes).await?;
        Ok(Address::Ipv6(bytes.into()))
    }

    async fn read_v6<T: AsyncRead + Unpin>(stream: &mut T) -> anyhow::Result<Self> {
        let mut bytes = [0; 4];
        stream.read_exact(&mut bytes).await?;
        Ok(Address::Ipv4(bytes.into()))
    }

    async fn read_domain<T: AsyncRead + Unpin>(stream: &mut T) -> anyhow::Result<Self> {
        let domain_length = stream.read_u8().await?;
        let mut bytes = String::with_capacity(domain_length as usize);
        let mut stream = stream.take(domain_length as u64);
        stream.read_to_string(&mut bytes).await?;
        Ok(Address::Domain(bytes.into_boxed_str()))
    }

    pub async fn read<T: AsyncRead + Unpin>(stream: &mut T) -> anyhow::Result<Self> {
        let atype = stream.read_u8().await?;

        match atype {
            0x01 => Self::read_v6(stream).await,
            0x03 => Self::read_domain(stream).await,
            0x04 => Self::read_v4(stream).await,
            _ => bail!("unsupported address type: {}", atype),
        }
    }

    pub async fn write<T: AsyncWrite + Unpin>(&self, stream: &mut T) -> anyhow::Result<()> {
        match self {
            Address::Ipv4(v4) => {
                stream.write_all(&[0x1]).await?;
                stream.write_all(v4.octets().as_ref()).await?;
                Ok(())
            }
            Address::Domain(domain) => {
                stream.write_all(&[0x3, domain.len().try_into()?]).await?;
                stream.write_all(domain.as_bytes()).await?;
                Ok(())
            }
            Address::Ipv6(v6) => {
                stream.write_all(&[0x4]).await?;
                stream.write_all(v6.octets().as_ref()).await?;
                Ok(())
            }
        }
    }
}

impl From<IpAddr> for Address {
    fn from(addr: IpAddr) -> Self {
        match addr {
            IpAddr::V4(v4) => Address::Ipv4(v4),
            IpAddr::V6(v6) => Address::Ipv6(v6),
        }
    }
}
